class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy, :token]
  # skip_before_filter :authenticate, :only => :token
  skip_before_filter :verify_authenticity_token
  
require "json"
require "uri"
require "net/http"


  # GET /sessions
  # GET /sessions.json
  def index
    @sessions = Session.all
  end

  # GET /sessions/1
  # GET /sessions/1.json
  def show
  end

  # GET /sessions/1
  # GET /sessions/1.json
  def token

    @hash = JSON.parse(@session.json)

    url = URI("http://rmorgan-latest.cafex.com:8080/gateway/sessions/session")

    request = Net::HTTP::Post.new(url.path, { "Content-Type" => "application.json", "Content-Length" => @hash.length().to_s() })
    request.body = @hash.to_json()

    httpClient = Net::HTTP.new(url.host, url.port).start()
    response = httpClient.request(request)

    @response = JSON.parse(response.body)["sessionid"]

    render :layout => 'token'

  end

  # GET /sessions/new
  def new
    @session = Session.new
  end

  # GET /sessions/1/edit
  def edit
  end

  # POST /sessions
  # POST /sessions.json
  def create
    @session = Session.new(session_params)

    respond_to do |format|
      if @session.save
        format.html { redirect_to @session, notice: 'Session was successfully created.' }
        format.json { render :show, status: :created, location: @session }
      else
        format.html { render :new }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sessions/1
  # PATCH/PUT /sessions/1.json
  def update
    respond_to do |format|
      if @session.update(session_params)
        format.html { redirect_to @session, notice: 'Session was successfully updated.' }
        format.json { render :show, status: :ok, location: @session }
      else
        format.html { render :edit }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sessions/1
  # DELETE /sessions/1.json
  def destroy
    @session.destroy
    respond_to do |format|
      format.html { redirect_to sessions_url, notice: 'Session was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      params.require(:session).permit(:json, :server, :user_id)
    end
end
