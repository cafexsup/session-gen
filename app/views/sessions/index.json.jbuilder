json.array!(@sessions) do |session|
  json.extract! session, :id, :json, :server, :user_id
  json.url session_url(session, format: :json)
end
