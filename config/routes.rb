Rails.application.routes.draw do

	mount Upmin::Engine => '/admin'
  root to: 'sessions#index'
  devise_for :users
  resources :users


  resources :sessions
  
  post "token/:id" => "sessions#token", as: :token

   
end
